package com.example.guess2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_gamezone.*

class Gamezone : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gamezone)
        chekii.setOnClickListener {
            if (answer.text.toString() == "hazard"){
                imageView.setImageResource(R.drawable.guti)
            }else if (
                answer.text.toString() == "guti"
            )
            {
                imageView.setImageResource(R.drawable.felix)
            }else if (
                answer.text.toString() == "felix"
            ){
                startActivity(Intent(this,Success::class.java))
            }else{
                startActivity(Intent(this,Gameover::class.java))

            }
        }

    }
}
