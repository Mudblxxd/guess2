package com.example.guess2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
private lateinit var auth:FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()
        reg.setOnClickListener {
            startActivity(Intent(this,RegActivity::class.java))
        }
        signIn.setOnClickListener {
            auth.signInWithEmailAndPassword(emailIn.text.toString(), passIn.text.toString())
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Toast.makeText(this, "succes", Toast.LENGTH_LONG).show()
                        startActivity(Intent(this,GuessActivity::class.java))
                    }
                    else {
                        Toast.makeText(this, "arasworia", Toast.LENGTH_LONG).show()
                    }
                }



        }
    }
}
