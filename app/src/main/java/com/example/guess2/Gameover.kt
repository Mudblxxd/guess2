package com.example.guess2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_gameover.*

class Gameover : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gameover)
        retry.setOnClickListener {
            startActivity(Intent(this,GuessActivity::class.java))
        }
    }
}
